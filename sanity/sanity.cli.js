import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'td9lgcqs',
    dataset: 'production'
  }
})
