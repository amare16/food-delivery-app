import { View, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import React, { useLayoutEffect, useEffect } from "react";
import { useRoute, useNavigation } from "@react-navigation/native";
import { urlFor } from "../sanity";
import { AntDesign } from "@expo/vector-icons";
import { Entypo, Ionicons, FontAwesome } from "@expo/vector-icons";
import DishRow from './../components/DishRow';
import BasketIcon from './../components/BasketIcon';
import { useDispatch } from 'react-redux';
import { setRestaurant } from '../features/restaurantSlice';

const RestaurantScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {
    params: {
      id,
      image,
      title,
      rating,
      genre,
      address,
      short_description,
      dishes,
      long,
      lat,
    },
  } = useRoute();

  useEffect(() => {
    dispatch(setRestaurant({
      id,
      image,
      title,
      rating,
      genre,
      address,
      short_description,
      dishes,
      long,
      lat,
    }))
  }, [dispatch])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  });
  return (
    <>
    <BasketIcon />
    <ScrollView className="relative">
      <View>
        <Image
          source={{
            uri: urlFor(image).url(),
            // uri: "https://lh5.googleusercontent.com/p/AF1QipOJC-eJx_e-Dk0l8pnc5HMSFGLf4KmD-U5FDQw=w203-h203-k-no"
          }}
          className="w-full h-56 bg-gray-300 p-10"
        />
        <TouchableOpacity
          onPress={navigation.goBack}
          className="absolute top-14 left-5 p-2 bg-gray-100 rounded-full"
        >
          <AntDesign name="arrowleft" size={24} color="#00CCBB" />
        </TouchableOpacity>
      </View>
      <View className="bg-white">
        <View className="px-4 pt-4">
          <Text className="text-3xl font-bold">{title}</Text>
          <View className="flex-row space-x-2 my-1">
            <View className="flex-row items-center space-x-1">
              <Entypo
                name="star-outlined"
                opacity={0.5}
                size={22}
                color="green"
              />
              <Text className="text-xs text-gray-500">
                <Text className="text-green-500">{rating}</Text> - {genre}
              </Text>
            </View>

            <View className="flex-row items-center space-x-1">
              <Ionicons
                name="location-outline"
                opacity={0.5}
                size={22}
                color="gray"
              />
              <Text className="text-xs text-gray-500">Near by - {address}</Text>
            </View>
          </View>
          <Text className="text-gray-500 mt-2 pb-4">{short_description}</Text>
        </View>

        <TouchableOpacity className="flex-row items-center space-x-2 p-4 border-y border-gray-300">
          <FontAwesome
            name="question-circle-o"
            size={20}
            color="gray"
            opacity={0.6}
          />
          <Text className="pl-2 flex-1 text-md font-bold">
            Have a food allergy
          </Text>
          <Entypo name="chevron-right" size={20} color="#00CCBB" />
        </TouchableOpacity>
      </View>
      <View className="pb-36">
        <Text className="px-4 pt-6 mb-3 font-bold text-xl">Menu</Text>

        {/* Dish rows */}
        {
          dishes.map(dish => (
            <DishRow 
              key={dish._id}
              id={dish._id}
              name={dish.name}
              description={dish.short_descrition}
              price={dish.price}
              image={dish.image}
            />
          ))
        }
      </View>
    </ScrollView>
    </>
  );
};

export default RestaurantScreen;
