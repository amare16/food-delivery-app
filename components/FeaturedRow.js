import { View, Text, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react';
import { AntDesign } from "@expo/vector-icons";
import RestaurantCard from './RestaurantCard';
import sanityClient from '../sanity';

const FeaturedRow = ({ id, title, description }) => {
  const [restaurants, setRestaurants] = useState([]);

  useEffect(() => {
    sanityClient.fetch(`
    *[_type == "featured" && _id == $id] {
      ...,
      restaurant[]->{
        ...,
        dishes[]->,
        type-> {
          short_description
        }
      }
    }[0]
    `,
    { id })
    .then((data) => {
      setRestaurants(data?.restaurant)
    })
  }, []);
console.log('restaurant: ' + restaurants)
  return (
    <View>
      <View className="mt-4 flex-row items-center justify-between px-4">
        <Text className="font-bold text-lg">{title}</Text>
        <AntDesign name="arrowright" size={24} color="#00CCBB" />
      </View>

      <Text className="text-xs text-gray-500 px-4">{description}</Text>

      <ScrollView
        horizontal
        contentContainerStyle={{
            paddingHorizontal: 15,
        }}
        showsHorizontalScrollIndicator={false}
        className="pt-4"
      >
        {/* RestaurantCards... */}
        {/* two ways of display..., spread operator [...] and object iteration */}

        {/* spread operator {...} */}
        {/* {
          restaurants.map((restaurant, index) => (
            <RestaurantCard key={index} {...restaurant}/>
          ))
        } */}
        {
          restaurants.map(restaurant => (
            <RestaurantCard 
            key={restaurant._id}
            id={restaurant._id}
            image={restaurant.image}
            title={restaurant.name}
            rating={restaurant.rating}
            genre={restaurant.genre}
            address={restaurant.address}
            short_description={restaurant.short_description}
            dishes={restaurant.dishes}
            long={restaurant.long}
            lat={restaurant.lat}
        />
          ))
        }
      </ScrollView>

    
    </View>
  )
}

export default FeaturedRow