import { View, Text, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react';
import CategoryCard from './CategoryCard';
import sanityClient, { urlFor } from '../sanity';

const Categories = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    sanityClient.fetch(`
      *[_type == "category"]
    `)
    .then((data) => {
      setCategories(data);
    })
  }, []);
  console.log("data category: ", categories)
  
  return (
    <ScrollView
        contentContainerStyle={{
            paddingHorizontal: 15,
            paddingTop: 10,
        }}
        horizontal
        showsHorizontalScrollIndicator={false}
    >   
    {/* CategoryCard */}   
      {
        categories.map(category => (
          <CategoryCard
          key={category._id}
          image={urlFor(category.image).width(200).url()}
          title={category.name}
        />
        ))
      }
        
     
    </ScrollView>
  )
}

export default Categories